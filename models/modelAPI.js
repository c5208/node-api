const dbname = process.env.DB_NAME
var ObjectId = require("mongodb").ObjectId
module.exports.getCoinData = (db) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = [
                {
                    $match: {}, // find({}) ดึงข้อมูลทั้งหมด
                },
            ]

            let col = db.db(dbname).collection("coin_transaction")
            let coinResult = await col.aggregate(query).toArray()

            if (coinResult.length !== 0) {
                resolve(coinResult)
            } else {
                resolve([])
            }
        } catch (error) {
            console.log(error)
            return resolve(false)
        }
    })
}