const dbname = process.env.DB_NAME
const _ = require('lodash')
var ObjectId = require("mongodb").ObjectId
module.exports.insertCoinDataTransaction = (db, dataInsert) => {
    return new Promise(async (resolve, reject) => {
        try {
            let col = db.db(dbname).collection("coin_transaction")
            let coinResult = await col.insertOne(dataInsert)
            // console.log(coinResult)
            if (coinResult.hasOwnProperty('insertedId')) {
                resolve(true)
            } else {
                resolve(false)
            }
        } catch (error) {
            console.log(error)
            return resolve(false)
        }
    })
}

module.exports.getTransactionCoinData = (db) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = [
                {
                    $match: {},
                },
                {
                    $sort: {
                        _id: -1,
                    },
                },
                {
                    $group: {
                        _id: "$symbol",
                        data: {
                            $push: "$$ROOT",
                        },
                    },
                },
            ]

            let col = db.db(dbname).collection("coin_transaction")
            let coinResult = await col.aggregate(query).toArray()
            // console.log(coinResult)  
            if (coinResult.length > 0) {
                resolve(coinResult)
            } else {
                resolve([])
            }
        } catch (error) {
            console.log(error)
            return resolve([])
        }
    })
}