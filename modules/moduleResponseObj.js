const errorCodeMsg = {
    10000: "ไม่สามารถเข้าถึงข้อมูลได้ กรุณาลองใหม่อีกครั้ง",
    10001: "ไม่สามารถเข้าถึงข้อมูลได้ พารามิเตอร์ไม่ครบ",
    90000: "Internal Server Error",
}
const successCodeMsg = {
    "00000": "success",
}
class RestResponse {
    errorMsgTable(code) {
        return errorCodeMsg[code]
    }
    successMsgTable(code) {
        return successCodeMsg[code]
    }
    success(successCode, body, operation) {
        if (typeof successCode != "undefined") {
            this.response_status = {
                code: successCode,
                operation: operation,
                message: this.successMsgTable[successCode],
                message_type: "",
                status: "SUCCESS",
            }
            this.result = body
        } else {
            throw new Error(`ReferenceError: "successCode" or "optName" is not defined`)
        }
    }
    error(errorCode, operation, result = "") {
        if (typeof errorCode != "undefined") {
            this.response_status = {
                code: errorCode.toString(),
                operation: operation,
                message: this.errorMsgTable(errorCode),
                message_type: "Information",
                status: "ERROR",
            }
            this.result = result
        } else {
            throw new Error(`ReferenceError: "successCode" or "optName" is not defined`)
        }
    }
}
module.exports.RestResponse = RestResponse
