const modelCrypto = require("../models/modelCrypto")
const moduleAxiosAPI = require("../modules/moduleAxiosAPI")

module.exports.insertCoinData = (db) => {
    return new Promise(async (resolve, reject) => {
        try {
            let coinData = await moduleAxiosAPI.getCoinMarketCapQuotesLatest()
            if (coinData.hasOwnProperty("data")) {
                for (const key in coinData.data) {
                    await modelCrypto.insertCoinDataTransaction(db, coinData.data[key])
                }
            }
            return resolve(true)
        } catch (error) {
            return resolve(false)
        }
    })
}
