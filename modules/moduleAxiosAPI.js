const axios = require('axios')
module.exports.getCoinMarketCapQuotesLatest = () => {
    return new Promise(async (resolve, reject) => {
        try {
            const config = {
                method: "get",
                url: "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol=BTC,ETH,USDT,BNB,ADA&convert=THB",
                headers: {
                    "X-CMC_PRO_API_KEY": "65a27a55-73c8-4dc9-a98b-f378425288a2",
                },
            }
            let response = await axios(config)
            if(response.hasOwnProperty('data')) {
                return resolve(response.data)
            }
        } catch (error) {
            return resolve([])
        }
    })
}
