const responseRestModule = require("../modules/moduleResponseObj").RestResponse
const modelCrypto = require('../models/modelCrypto')
const Joi = require("joi")
const _ = require("lodash")
module.exports.getTransactionCoinData = async (req, res) => {
    let response = new responseRestModule()
    let serviceName = "getTransactionCoinData"
    try {
        // let responseData = {
        //     message: "Hello World"
        // }
        // Bussines Logic (call Modules or Models)
        let responseData = await modelCrypto.getTransactionCoinData(req["mgdb"])
        response.success('00000',responseData, serviceName)
        return res.send(response)
    } catch (err) {
        response.error(90000,serviceName)
        return res.send(response)
    }
}