const express = require('express');
const router = express.Router();
let middlewareCors = require('./middlewareCors');
let middlewareBodyParser = require('./middlewareBodyParser');
let middlewareMongo = require('./middlewareMongo');
router.use('/', middlewareCors);
router.use('/', middlewareBodyParser);
router.use('/', middlewareMongo);
module.exports = router;