const express = require("express")
const mongoDB = require("../modules/moduleMongo")
const router = express.Router()
const responseRestModule = require("../modules/moduleResponseObj").RestResponse
let connection = null
router.use("*", async (req, res, next) => {
    if (connection !== null) {
        req["mgdb"] = connection
        return next()
    }
    try {
        connection = await new mongoDB()
        req["mgdb"] = connection
        return next()
    } catch (err) {
        console.log(err)
        let response = new responseRestModule()
        response.error("90000", "Middleware Mongodb")
        res.send(response)
    }
})
module.exports = router
