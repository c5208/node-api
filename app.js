const env = require("dotenv").config({})
const express = require("express")
const app = express()
// import middlewares
const middleware = require("./middlewares/middlewareIndex")
app.use("/", middleware)
app.use(express.json({ limit: "50mb" }))

// import router
const router = require("./routes/routeIndex") // import route
app.use("/", router)
// app.get("/", (req, res) => {
//     res.send("Hello World")
// })

app.disable("x-powered-by")
app.listen(process.env.EXPRESS_PORT, async () => {
    console.log(`App listening on port ${process.env.EXPRESS_PORT}!`)
})

// import cronJob
const dayjs = require("dayjs")
const cronjob = require("cron").CronJob
const MongoClient = require("mongodb").MongoClient
const moduleCrppto = require('./modules/moduleCrypto')
const cronDemo = async () => {
    try {
        const job = new cronjob("0 */30 * * * *", async () => {
            const mgdb = await MongoClient.connect(process.env.MONGO_URL)
            console.log("job", dayjs(new Date(), "Asia/Bangkok").format("YYYY-MM-DD HH:mm:ss"))
            await moduleCrppto.insertCoinData(mgdb)
            mgdb.close()
        })
        job.start()
        return true
    } catch (error) {
        console.log("ERROR job", dayjs(new Date(), "Asia/Bangkok").format("YYYY-MM-DD HH:mm:ss"), error)
        return false
    }
}

;(async () => {
    console.log("CronJob")
    await cronDemo()
})()
