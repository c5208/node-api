const express = require("express");
const router = express.Router();
const routeAPI = require("./routeAPI")
router.use("/api/", routeAPI)
router.use(`*`, (req, res) => res.status(404).end())
module.exports = router
