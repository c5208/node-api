const express = require('express');
const router = express.Router();
const controllerAPI = require('../controllers/controllerAPI');

// API
router.get('/lists', controllerAPI.getTransactionCoinData);
module.exports = router;